//
//  CKFObserver.swift
//  CKFoundation
//
//  Created by Diego on 8/30/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import Foundation

public struct CKFObserver {
    
    public struct Value<Input> {
        
        public var callback: ((Input) -> Void)?
        
        public var value: Input {
            didSet { callback?(value) }
        }
        
        public init(value: Input) {
            self.value = value
        }
        
        public mutating func observe<Object: AnyObject> (_ object: Object, with callback: @escaping (Object, Input) -> Void) {
            self.callback = { [weak object] input in
                guard let object = object else { return }
                callback(object, input)
            }
        }
        
    }
    
    public struct ValueOutput<Input, Output> {
        
        public var callback: ((Input) -> Output?)?
        
        public var value: Input {
            didSet {
                self.result = callback?(value)
            }
        }
        
        public var result: Output?
        
        public mutating func observe<Object: AnyObject> (_ object: Object, with callback: @escaping (Object, Input) -> Output?) {
            self.callback = { [weak object] input in
                guard let object = object else { return nil }
                return callback(object, input)
            }
        }
    }
    
}
