//
//  Messages.swift
//  CKFoundation
//
//  Created by Diego on 8/30/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import Foundation

public struct CKFExample {
    public func test() {
        print("123")
    }
}

public struct CKFMessage {
    
    public struct Passthrough<Input> {
        
        public init() { }
        
        public var callback: ((Input) -> Void)?
        
        public mutating func observe<Object: AnyObject> (_ object: Object, with callback: @escaping (Object, Input) -> Void) {
            self.callback = { [weak object] input in
                guard let object = object else { return }
                callback(object, input)
            }
        }
        
    }
    
    public struct PassthroughOutput<Input, Output> {
        
        public init() { }
        
        public var callback: ((Input) -> Output?)?
        
        public mutating func observe<Object: AnyObject> (_ object: Object, with callback: @escaping (Object, Input) -> Output?) {
            self.callback = { [weak object] input in
                guard let object = object else { return nil }
                return callback(object, input)
            }
        }
        
    }
    
}

