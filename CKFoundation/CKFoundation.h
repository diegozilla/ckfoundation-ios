//
//  CKFoundation.h
//  CKFoundation
//
//  Created by Diego on 8/30/19.
//  Copyright © 2019 Diego. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CKFoundation.
FOUNDATION_EXPORT double CKFoundationVersionNumber;

//! Project version string for CKFoundation.
FOUNDATION_EXPORT const unsigned char CKFoundationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CKFoundation/PublicHeader.h>


